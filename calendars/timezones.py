#
# Copyright 2022, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Timezone support utils
"""

from collections import defaultdict
from dateutil.tz import gettz
from dateutil.zoneinfo import get_zonefile_instance

ALL_ZONES = list(get_zonefile_instance().zones)
DST_CHOICES = [('UTC', 'UTC (No DST)')]
DST_UNIQUE = defaultdict(list)
DST_INFO = dict()

def is_numeric(abbr):
    abbr = abbr.replace('-', '').replace('+', '')
    return abbr.isnumeric()

# Make the choices a unique list using the smallest name
for zone in ALL_ZONES:
    tz = gettz(zone)
    (std, dst) = tz._ttinfo_std, tz._ttinfo_dst

    if not std or not dst:
        continue
    if is_numeric(std.abbr) or is_numeric(dst.abbr):
        continue

    DST_UNIQUE[(std.abbr, dst.abbr)].append(zone)
    DST_INFO[std.abbr] = std
    DST_INFO[dst.abbr] = dst


#_ttinfo(offset=-14400, delta=datetime.timedelta(-1, 72000), isdst=1, abbr='EDT', isstd=False, isgmt=False, dstoffset=datetime.timedelta(0, 3600))
def get_sort(infos):
    return DST_INFO[infos[0]].offset

def get_label(info):
    hour = int(info.offset / 60 / 60)
    minute = int(info.offset / 60 % 60)
    return f"{info.abbr} {hour:d}:{minute:02d}"

for std, dst in sorted(DST_UNIQUE.keys(), key=get_sort):
    std_lbl = get_label(DST_INFO[std])
    dst_lbl = get_label(DST_INFO[dst])
    for zone in DST_UNIQUE[(std, dst)]:
        DST_CHOICES.append((zone, f"{zone} - {std_lbl} / {dst_lbl}"))
    



