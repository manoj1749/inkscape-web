# Cat and bird avatar images

All images in this directory have been created by David Revoy and are licensed under the [Creative Commons CC-By 4.0 license](https://creativecommons.org/licenses/by/4.0/), with the following exception: if used as avatars (for blog, forum, social-network), a direct attribution is not needed, so they can be used as regular avatars without pasting David Revoy's name all over the place.

Find the original source images here: https://framagit.org/Deevad/cat-avatar-generator
