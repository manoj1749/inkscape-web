#
# Copyright 2022, Martin Owens <doctormo@gmail.com>
#
# This file is part of the software inkscape-web, consisting of custom
# code for the Inkscape project's django-based website.
#
# inkscape-web is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# inkscape-web is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with inkscape-web.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Admin interfaces for budget.
"""

from django.utils.safestring import mark_safe
from django.contrib.admin import ModelAdmin, register, TabularInline
from django.forms.models import BaseInlineFormSet
from django.utils.timezone import now

from .models import Budget, BudgetCategory, BudgetEntry, EntryValue

class CategoryInline(TabularInline):
    model = BudgetCategory

@register(Budget)
class BudgetAdmin(ModelAdmin):
    list_display = ('name', 'manager', 'last_update')
    raw_id_fields = ('manager',)
    inlines = [CategoryInline]

    def last_update(self, instance):
        latest = instance.entries.latest()
        if latest:
            return latest.point
        return 'None'


class ValueInlineFormSet(BaseInlineFormSet):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        obj = kwargs['instance']
        if 'data' not in kwargs and obj.pk and obj.data.count() == 0:
            self.inital = self.get_initial(obj)

    def get_initial(self, obj):
        self.initial = []
        for cat in obj.budget.categories.filter(is_active=True):
            self.initial.append({'category': cat})
            if cat.is_previous:
                previous = obj.get_previous()
                if previous:
                    self.initial[-1]['value'] = previous.get_total()

class ValueInline(TabularInline):
    model = EntryValue
    fields = ('category', 'value')
    can_delete = False
    formset = ValueInlineFormSet

    def get_readonly_fields(self, request, obj=None):
        if obj:
            orig_obj = BudgetEntry.objects.get(pk=obj.pk)
            if obj.is_locked or orig_obj.is_locked:
                return ('category', 'value')
        return ()

    def get_extra(self, request, obj=None):
        return self.get_max_num(request, obj=obj)

    def get_max_num(self, request, obj=None):
        if not obj or obj.is_locked:
            return 0
        return obj.budget.categories.filter(is_active=True).count()

@register(BudgetEntry)
class BudgetEntryAdmin(ModelAdmin):
    list_display = ('point', 'budget', 'get_total', 'created_by', 'created_on', 'is_locked')
    list_filter = ('point', 'budget')
    raw_id_fields = ('created_by',)
    readonly_fields = ('total', 'created_on', 'created_by')

    inlines = [ValueInline]

    def get_readonly_fields(self, request, obj=None):
        if obj:
            return self.readonly_fields + ('point', 'budget')
        return self.readonly_fields + ('is_locked',)

    def get_changeform_initial_data(self, request):
        return {'created_on': now(), 'created_by': request.user}

    def render_change_form(self, request, context, add=False, change=False, form_url='', obj=None):
        if add:
            context['show_save'] = False
            context['show_save_and_continue'] = True
        else:
            context['show_save'] = True
            context['show_save_and_continue'] = False
        if change and obj.is_locked:
            context['show_delete'] = False
        context['show_save_and_add_another'] = False # django 3.1 and above
        return super().render_change_form(request, context, add, change, form_url, obj)
