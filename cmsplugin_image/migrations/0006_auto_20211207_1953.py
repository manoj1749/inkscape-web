# Generated by Django 2.0 on 2021-12-07 19:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cmsplugin_image', '0005_auto_20180504_2002'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='page_link',
            field=models.ForeignKey(blank=True, help_text='If present, clicking on image will take user to specified cms page.', limit_choices_to={'publisher_is_draft': True}, null=True, on_delete=django.db.models.deletion.SET_NULL, to='cms.Page', verbose_name='CMS page link'),
        ),
    ]
